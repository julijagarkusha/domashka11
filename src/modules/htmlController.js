import { getFilterButtonLabel } from './utils.js';

const tasksFilterButtons = document.querySelectorAll('.tasks__filter .btn');

export const createNode = (element, classList = '') => {
  const domElement = document.createElement(element);
  if (classList.length) {
    domElement.setAttribute('class', classList);
  }

  return domElement;
};

const createTaskButton = (classList, iconPath, iconName, clickHandler) => {
  const button = createNode('button', classList);

  if (iconPath) {
    const icon = createNode('img');
    icon.setAttribute('src', iconPath);
    icon.setAttribute('alt', iconName);
    icon.classList.add('task__icon');
    button.appendChild(icon);
  }

  button.addEventListener('click', clickHandler);
  return button;
}

export const createTaskElement = (task, taskRef) => {
  const taskElementClasses = task.isDone ? 'task__text task__text--closed' : 'task__text';
  const taskContainerElement = createNode('article', 'task__item');
  const taskNameElement = createNode('p', taskElementClasses);
  taskNameElement.innerText = task.name;
  const taskActionsElement = createNode('div', 'task__actions');

  const completedButton = createTaskButton(
    task.isDone ? 'btn btn-success task__btn task__btn--closed' : 'btn btn-success task__btn',
    task.isDone ? './assets/icons/check.svg' : '',
    'completed task icon',
    () => task.isDone ? task.setInProgress() : task.setCompleted(),
  );

  const removeButton = createTaskButton(
    'btn btn-danger task__btn',
    './assets/icons/close-icon.svg',
    'remove task icon',
    () => taskRef.removeTask(task.id),
  );

  taskContainerElement.appendChild(taskNameElement);
  taskContainerElement.appendChild(taskActionsElement);

  taskActionsElement.appendChild(completedButton);
  taskActionsElement.appendChild(removeButton);

  return taskContainerElement;
};

export const updateHTML = (tasksList, tasksRef) => {
  const tasksListElement = document.querySelector('.task__list');
  tasksListElement.innerHTML = '';
  tasksList.forEach(task => {
    tasksListElement.prepend(createTaskElement(task, tasksRef));
  });
  tasksFilterButtons.forEach(tasksFilterButton => {
    tasksFilterButton.innerText = getFilterButtonLabel(tasksFilterButton, tasksRef);
  });
};
