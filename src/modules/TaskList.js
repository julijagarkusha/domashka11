import { Task } from './Task.js';

export function TaskList(onUpdateHandler = () => {
}) {
  this.tasksList = [];

  this.createTask = function (name) {
    this.tasksList.push(new Task(name, onUpdateHandler));
    onUpdateHandler();
  };

  this.removeTask = function (id) {
    this.tasksList = this.tasksList.filter(item => item.id !== id);
    onUpdateHandler();
  };

  Object.defineProperties(this, {
    inProgress: { get: () => this.tasksList.filter(item => !item.isDone) },
    completed: { get: () => this.tasksList.filter(item => item.isDone) },
    all: { get: () => this.tasksList },
  });
}
