export function Task(name, onUpdateHandler) {
  this.id = Date.now();
  this.name = name;
  this.isDone = false;

  this.setCompleted = function () {
    this.isDone = true;
    onUpdateHandler();
  };

  this.setInProgress = function () {
    this.isDone = false;
    onUpdateHandler();
  };
}
