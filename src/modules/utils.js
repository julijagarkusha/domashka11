export const getFilterButtonLabel = (button, tasksRef) => {
  return `${ button.getAttribute('data-label') } (${ tasksRef[button.getAttribute('data-filter')].length })`;
};
